                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:3349425
Silent Auto Home for Ender-5 by lgaga is licensed under the Creative Commons - Attribution - Share Alike license.
http://creativecommons.org/licenses/by-sa/3.0/

# Summary

X/Y Axis End Stop Switch Holder for Ender-5 3D Printer. Silent X/Y Axis Auto Home function.


With Auto Home's factory limit switch, the X and Y cars collide with the frame. This is not useful.
This solution corrects this error.

Can be installed in place of factory elements.

The location of the M3 screws is useful for expanding and drilling threads.

Available editable files:
https://gallery.autodesk.com/fusion360/projects/135873/xy-axis-end-stop-switch-holder-for-ender-5-3d-printer



- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

<B>Upgrade (2019.03.08)</B>
Upon request, the Microswitch holders with larger holes.
Y_Axis_End_Stop_Swtich_Holder_v22.stl
X_Axis_End_Stop_Switch_Holder_v11 .stl

# Print Settings

Printer Brand: Creality
Printer: Ender 5
Rafts: Doesn't Matter
Supports: Doesn't Matter
Resolution: 0,2
Infill: 20%
Filament_brand: Devil Design
Filament_color: Black
Filament_material: PLA

Notes: 
Print it, put the switch on it.
90 degrees turn and fasten.(!)

At work.
Thanks for the video of John Brian Silverion.
https://www.facebook.com/silverph/videos/10156500215452872/