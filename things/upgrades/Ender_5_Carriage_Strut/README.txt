                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:3280199
Ender 5 Carriage Strut by akaindy is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

Got the Ender 5 to use along with may CR10S5. Really like the simple drive system they came up with so I figured I'd give it a try even though its brand new. One thing I noticed when doing the first prints was that the carriage had a little springboard effect to it, otherwise this printer is really nice, hope they scale it up bigger. So to fix the carriage I designed up this strut. It might be a little bit of over kill I had my 14yr old son stand on the first test version and it didn't phase it. It clamps to the outside z-axis bearings using 4 6-32x.500 Button Head screws and 4 hex nuts per strut. Leave it loosely clamped and then snap it onto the inside of the carriage cutouts and then tighten the screws. Level the bed and your good to go. After I calibrated the printer it is producing amazing surface finish and accuracy. I printed them laying down unlike the STL view that shows them standing up. 

Updates 12-17-2018: Added Strut files that are modified to use M4 Screws and Hex Nuts.

Updates 1-8-2019: Added the Clamp plates in both Metric and Inches to the STL files as just separate files so if they are needed they can be printed separately. 

# Print Settings

Printer Brand: Creality
Printer: Ender 5
Rafts: No
Supports: Yes
Resolution: .2mm layer, 1.2mm wall thickness
Infill: 50%

Notes: 
Also used the Alternate extra wall setting in Cura.